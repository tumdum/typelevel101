module List where
import Prelude hiding (foldr, concat)

data List a = Cons a (List a) | Nil deriving (Show, Eq)

list :: [a] -> List a
list [a] = Cons a Nil
list (h:t) = Cons h $ list t

len :: List a -> Int
len Nil = 0
len (Cons _ t) = 1 + len t

reduce :: Num a => List a -> a
reduce (Cons h Nil) = h
reduce (Cons h t)   = h + (reduce t)

foldr :: (a -> b -> b) -> b -> List a -> b
foldr _ acc Nil = acc
foldr f acc (Cons h t) = f h newacc
    where newacc = foldr f acc t

filterAdapter :: (a -> Bool) -> a -> List a -> List a
filterAdapter p val acc = if p val then (Cons val acc) else acc

filter :: (a -> Bool) -> List a -> List a
filter p l = foldr (filterAdapter p) Nil l

concat :: List a -> List a -> List a
concat Nil rhs = rhs
concat (Cons h t) rhs = Cons h $ concat t rhs

not :: (a -> Bool) -> (a -> Bool)
not p = \x -> if p x then False else True

sort :: Ord a => List a -> List a
sort Nil = Nil
sort (Cons h t) = let
    less = List.filter (<h) t
    more = List.filter (List.not (<h)) t
    sortedLess = sort less
    sortedMore = sort more
    in concat sortedLess (Cons h sortedMore)

sortBy :: (a -> a -> Bool) -> List a -> List a
sortBy _ Nil = Nil
sortBy p (Cons h t) = let
    less = List.filter (\x -> p x h) t
    more = List.filter (List.not (\x -> p x h)) t
    sortedLess = sortBy p less
    sortedMore = sortBy p more
    in concat sortedLess (Cons h sortedMore)

sort' :: Ord a => List a -> List a
sort' = sortBy (<)
