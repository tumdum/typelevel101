module Number where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

data Number = 
    Zero | 
    Next Number
    deriving (Show, Eq)

v :: Int -> Number
v 0 = Zero
v n = Next $ v $ n -1

add :: Number -> Number -> Number
add Zero       rhs = rhs
add (Next lhs) rhs = Next (add lhs rhs)

mul :: Number -> Number -> Number
mul Zero        _   = Zero
mul (Next lhs)  rhs = add rhs (mul lhs rhs)

less :: Number -> Number -> Bool
less Zero (Next _)         = True
less (Next _) Zero         = False
less Zero Zero             = False
less (Next lhs) (Next rhs) = less lhs rhs

eq :: Number -> Number -> Bool
eq Zero Zero             = True
eq (Next lhs) (Next rhs) = eq lhs rhs
eq _ _                   = False
