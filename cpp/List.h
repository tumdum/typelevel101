#ifndef LIST_H_
#define LIST_H_

template <typename H, typename T>
struct TCons
{
    using Head = H;
    using Tail = T;
};

struct TNil {};

template <typename H, typename ... T>
struct TList;

template <typename H>
struct TList<H>
{
    using type = TCons<H, TNil>;
};

template <typename H, typename ... T>
struct TList
{
    using type = TCons<H, typename TList<T...>::type>;
};

template <typename ... Args>
using List = typename TList<Args...>::type;

// ==== LEN ====
template <typename L>
struct Len;

template <>
struct Len<TNil>
{
    using type = Zero;
    enum { value = 0 };
};

template <typename H, typename T>
struct Len<TCons<H, T>>
{
    using type = Add<Next<Zero>, typename Len<T>::type>;
    enum { value = 1 + Len<T>::value };
};

// ==== SUM ====
template <typename L>
struct Reduce;

template <typename T>
struct Reduce<TCons<T, TNil>>
{
    enum { value = T::value };
};

template <typename H, typename T>
struct Reduce<TCons<H, T>>
{
    enum { value = H::value + Reduce<T>::value };
};

// ==== FOLDR ====
template <typename F, typename Acc, typename L>
struct Foldr;

template <typename F, typename Acc>
struct Foldr<F, Acc, TNil>
{
    using type = Acc;
};

template <typename F, typename Acc, typename H, typename T>
struct Foldr<F, Acc, TCons<H, T>>
{
    using NewAcc = typename Foldr<F, Acc, T>::type;
    using type = typename F::template Value<H, NewAcc>::type;
};

struct AddFun {
    template <typename Val, typename Acc>
    struct Value {
        using type = Add<Val, Acc>;
    };
};

struct LenFun {
    template <typename Val, typename Acc>
    struct Value {
        using type = Add<Next<Zero>, Acc>;
    };
};

struct ProdFun {
    template <typename Val, typename Acc>
    struct Value {
        using type = Mul<Val, Acc>;
    };
};

// ==== COND ====

template <typename P, typename T, typename F>
struct lazy_if;

template <typename T, typename F>
struct lazy_if<std::true_type, T, F>
{
    using type = T;
};

template <typename T, typename F>
struct lazy_if<std::false_type, T, F>
{
    using type = F;
};

// ==== FILTER ====

template <template<class> class P>
struct FilterAdapter
{
    template <typename Val, typename Acc>
    struct Value
    {
        using type = typename lazy_if<typename P<Val>::type,
              TCons<Val, Acc>, Acc>::type;
    };
};

template <template<class> class P, typename L>
struct Filter
{
    using type = typename Foldr<FilterAdapter<P>, TNil, L>::type;
};

template <template <class> class P>
struct Not
{
    template <typename Arg>
    struct Value
    {
        using type = typename lazy_if<typename P<Arg>::type,
            std::false_type, std::true_type>::type;
    };
};

template <typename LHS, typename RHS>
struct Concat;

template <typename RHS>
struct Concat<TNil, RHS>
{
    using type = RHS;
};

template <typename H, typename T, typename RHS>
struct Concat<TCons<H, T>, RHS>
{
    using type = TCons<H, typename Concat<T, RHS>::type>;
};


template <template <class, class> class F, typename RHS>
struct ApplyOnlySecond
{
    template <typename LHS>
    struct Value
    {
        using type = typename F<LHS, RHS>::type;
    };
};

template <template <class, class> class Pred, typename L>
struct _SortBy;

template <template <class, class> class Pred>
struct _SortBy<Pred, TNil>
{
    using type = TNil;
};

template <template <class, class> class Pred, typename H, typename T>
struct _SortBy<Pred, TCons<H, T>>
{
    using less = typename Filter<ApplyOnlySecond<Pred, H>::template Value, T>::type;
    using more = typename Filter<Not<ApplyOnlySecond<Pred, H>::template Value>::template Value, T>::type;

    using sortedLess = typename _SortBy<Pred, less>::type;
    using sortedMore = typename _SortBy<Pred, more>::type;

    using type = typename Concat<sortedLess, TCons<H, sortedMore>>::type;
};

template <template <class, class> class Pred, typename L>
using SortBy = typename _SortBy<Pred, L>::type;

template <typename L>
using Sort = SortBy<Less, L>;

#endif  // LIST_H_
