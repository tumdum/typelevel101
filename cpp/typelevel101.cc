#include <cassert>
#include <type_traits>
#include <cstring>

#include "Num.h"
#include "List.h"
#include "Vector.h"

template <typename A, typename B>
struct equal_types
{
    static_assert(std::is_same<A, B>::value, "");
};

using One    = Next<Zero>;
using Two    = Next<One>;
using Three  = Next<Two>;
using Four   = Next<Three>;
using Five   = Add<Three, Two>;
using Six    = V<6>;
using Seven  = V<7>;
using Ten    = Add<Five, Five>;
using Eleven = Add<Five, Add<Five, One>>;

using l1 = TCons<Five, TCons<Three, TCons<Two, TCons<One, TNil>>>>;
using l2 = List<Five, Three, Two, One>;

template <typename N>
struct _Fib;

template<>
struct _Fib<One> {
    using type = One;
};

template<>
struct _Fib<Two> {
    using type = One;
};

template<typename Prev>
struct _Fib<Next<Next<Prev>>> {
    using type = Add<
        typename _Fib<Next<Prev>>::type,
        typename _Fib<Prev>::type>;
};

template <typename N>
using Fib = typename _Fib<N>::type;

template <typename Arg>
struct less5Pred
{
    using type = Less<Arg, V<5>>;
};

template <typename LHS, typename RHS>
struct SizeofLess
{
    using type = Less<V<sizeof(LHS)>, V<sizeof(RHS)>>;
};

int main() {
    assert(0 == Zero::value && 0 == V<0>::value);
    assert(2 == Next<Next<Zero>>::value && 2 == V<2>::value);
    assert(5 == Five::value && 5 == V<5>::value);

    equal_types<V<23>, Add<V<23>, V<0>>>();
    equal_types<V<23>, Add<V<4>, V<19>>>();

    static_assert(std::is_same<V<42>, Mul<Six, Seven>>::value, "");
    equal_types<V<15>, Mul<V<5>, V<3>>>();
    equal_types<V<99>, Mul<V<11>, V<9>>>();
    equal_types<V<900>, Mul<V<30>, V<30>>>();

    static_assert(std::is_same<std::true_type, Less<V<7>, V<8>>>::value, "");
    static_assert(!std::is_same<std::true_type, Less<V<9>, V<8>>>::value, "");
    static_assert(!std::is_same<std::true_type, Less<V<9>, V<9>>>::value, "");
    equal_types<std::true_type, Less<V<0>, V<1>>>();
    equal_types<std::false_type, Less<V<1>, V<0>>>();
    equal_types<std::true_type, Less<V<500>, V<600>>>();
    equal_types<std::false_type, Less<V<999>, V<600>>>();

    equal_types<std::true_type, Eq<Zero, Zero>>();
    equal_types<std::true_type, Eq<V<543>, V<543>>>();
    equal_types<std::false_type, Eq<V<2>, V<1>>>();
    equal_types<std::false_type, Eq<V<2>, V<11>>>();

    equal_types<Five, l1::Head>();
    equal_types<Three, l1::Tail::Head>();
    equal_types<Two, l1::Tail::Tail::Head>();
    equal_types<One, l1::Tail::Tail::Tail::Head>();
    equal_types<l1, l2>();

    static_assert(std::is_same<Four, typename Len<l1>::type>::value, "");

    assert(4 == Len<l1>::value);
    assert(11 == Reduce<l1>::value);
    assert(11 == Reduce<l2>::value);

    const auto v1 = Cons(1, VEmpty);
    const auto v2 = Cons(10, v1);
    const auto v3 = Cons(100, v2);

    const auto w3 = Cons(200, Cons(20, Cons(2, VEmpty)));

    const auto expected = 300 %= 30 %= 3 %= VEmpty;
    
    const auto result = w3 + v3;
    assert(expected == result);

    using L1Sum = Foldr<AddFun, Zero, l1>::type;
    static_assert(std::is_same<L1Sum, Eleven>::value, "");

    using L1Len = Foldr<LenFun, Zero, l1>::type;
    static_assert(std::is_same<Four, L1Len>::value, "");

    using L1Prod = Foldr<ProdFun, One, l1>::type;
    static_assert(std::is_same<V<30>, L1Prod>::value, "");

    static_assert(std::is_same<V<144>, Fib<V<12>>>::value, "");
    static_assert(std::is_same<V<55>, Fib<Ten>>::value, "");
    // comment next line if compiling with clang
    static_assert(std::is_same<V<46368>, Fib<V<24>>>::value, "");

    using l3 = List<V<7>, V<5>, V<1>, V<4>, V<5>, V<6>, V<10>, V<7>, V<3>>;
    using less5 = typename Filter<ApplyOnlySecond<Less, V<5>>::Value, l3>::type;
    equal_types<List<V<1>, V<4>, V<3>>, less5>();
    using sortedl3 = Sort<l3>;
    using expectedSortedl3 = List<V<1>, V<3>, V<4>, V<5>, V<5>, V<6>, V<7>, V<7>, V<10>>;
    equal_types<expectedSortedl3, sortedl3>();
    using types = List<int, char, int[40], long long>;
    using sortedTypes = SortBy<SizeofLess, types>;
    using exptectedSortedTypes = List<char, int, long long, int[40]>;
    equal_types<exptectedSortedTypes, sortedTypes>();

    static_assert(!std::is_same<Five, Two>::value, "");
    static_assert(std::is_same<Add<Four, Four>, Add<Five, Add<Two, One>>>::value, "");
}
