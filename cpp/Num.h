#ifndef NUM_H_
#define NUM_H_

#include <type_traits>

struct Zero
{
    enum { value = 0 };
};

template <typename Nat>
struct Next 
{
    enum { value = 1 + Nat::value };
};

template <int i>
struct _V { using type = Next<typename _V<i-1>::type>; };

template <>
struct _V<0> { using type = Zero; };

template <int i>
using V = typename _V<i>::type;

// ==== ADD ====
template <typename Lhs, typename Rhs> struct _Add;

template <typename Rhs> struct _Add<Zero, Rhs>
{
    using type = Rhs;
};

template <typename Lhs, typename Rhs> struct _Add<Next<Lhs>, Rhs>
{
    using type = Next<typename _Add<Lhs, Rhs>::type>;
};

template <typename LHS, typename RHS>
using Add = typename _Add<LHS, RHS>::type;

// ==== MUL ====
template <typename Lhs, typename Rhs> struct _Mul;

template <typename Rhs> struct _Mul<Zero, Rhs>
{
    using type = Zero;
};

template <typename Lhs, typename Rhs> struct _Mul<Next<Lhs>, Rhs>
{
    using tmp = typename _Mul<Lhs, Rhs>::type;
    using type = typename _Add<Rhs, tmp>::type;
};

template <typename LHS, typename RHS>
using Mul = typename _Mul<LHS, RHS>::type;

// ==== LESS ====
template <typename LHS, typename RHS>
struct _Less;

template <typename RHS>
struct _Less<Zero, Next<RHS>>
{
    using type = std::true_type;
};

template <typename LHS>
struct _Less<Next<LHS>, Zero> 
{
    using type = std::false_type;
};

template <>
struct _Less<Zero, Zero> 
{
    using type = std::false_type;
};

template <typename LHS, typename RHS>
struct _Less<Next<LHS>, Next<RHS>>
{
    using type = typename _Less<LHS, RHS>::type;
};

template <typename LHS, typename RHS>
using Less = typename _Less<LHS, RHS>::type;

// ==== Eq ====
template <typename LHS, typename RHS>
struct _Eq
{
    using type = std::false_type;
};

template <>
struct _Eq<Zero, Zero>
{
    using type = std::true_type;
};

template <typename LHS, typename RHS>
struct _Eq<Next<LHS>, Next<RHS>>
{
    using type = typename _Eq<LHS, RHS>::type;
};


template <typename LHS, typename RHS>
using Eq = typename _Eq<LHS, RHS>::type;

#endif  // NUM_H_
