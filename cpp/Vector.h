#ifndef VECTOR_H_
#define VECTOR_H_

template <typename TailSize>
struct Vector;

template <typename TailSize>
struct Vector<Next<TailSize>>
{
    using size = Next<TailSize>;

    int head;
    Vector<TailSize> tail;
};

template <>
struct Vector<Zero>
{
    using size = Zero;
};

const Vector<Zero> VEmpty{};

template <typename TailSize>
Vector<Next<TailSize>> Cons(const int head, const Vector<TailSize>& tail)
{
    return Vector<Next<TailSize>>{head, tail};
}

template <typename TailSize>
Vector<Next<TailSize>> operator %= (
        const int head, 
        const Vector<TailSize>& tail)
{
    return Vector<Next<TailSize>>{head, tail};
}

template <typename TailSize>
Vector<Next<TailSize>> operator + (
        const Vector<Next<TailSize>>& lhs,
        const Vector<Next<TailSize>>& rhs)
{
    return Cons(lhs.head + rhs.head, lhs.tail + rhs.tail);
}

Vector<Zero> operator + (const Vector<Zero>&, const Vector<Zero>&)
{
    return VEmpty;
}

template <typename TailSize>
bool operator == (
        const Vector<Next<TailSize>>& lhs,
        const Vector<Next<TailSize>>& rhs)
{
    return lhs.head == rhs.head && lhs.tail == rhs.tail;
}

bool operator == (const Vector<Zero>&, const Vector<Zero>&) { return true; }


#endif  // VECTOR_H_
